#!/usr/bin/env python3
from cumulative_library import timing

# inclusive
LIMIT = 1000


def get_digits(n):
    return [n] if n < 10 else get_digits(n // 10) + [n % 10]


def convert(n):
    units = {
        i: word
        for i, word in enumerate(
            ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
        )
    }
    teens = {
        i: word
        for i, word in enumerate(
            [
                "ten",
                "eleven",
                "twelve",
                "thirteen",
                "fourteen",
                "fifteen",
                "sixteen",
                "seventeen",
                "eighteen",
                "nineteen",
            ]
        )
    }
    tens = {
        i: word
        for i, word in enumerate(
            [
                "twenty",
                "thirty",
                "forty",
                "fifty",
                "sixty",
                "seventy",
                "eighty",
                "ninety",
            ],
            start=2,
        )
    }
    tens[0] = ""
    digits = list(reversed(get_digits(n)))
    if n < 10:
        return units[n]
    if 9 < n < 20:
        return teens[digits[0]]
    if 19 < n < 100:
        return tens[digits[1]] + units[digits[0]]
    if 99 < n < 1000:
        if not digits[1] and not digits[0]:
            return units[digits[2]] + "hundred"
        else:
            if digits[1] == 1:
                return units[digits[2]] + "hundredand" + teens[digits[0]]
            else:
                return (
                    units[digits[2]] + "hundredand" + tens[digits[1]] + units[digits[0]]
                )
    if n == 1000:
        return "onethousand"


@timing
def summation(limit=LIMIT):
    return sum([len(convert(x)) for x in range(1, limit + 1)])


if __name__ == "__main__":
    print(summation())
