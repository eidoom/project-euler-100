#!/usr/bin/env python3
from cumulative_library import timing, list_primes

# slow

LIMIT = 10**6


@timing
def circular_primes(limit=LIMIT):
    primes = list_primes(limit)
    # number = 3
    # for prime in primes[3:]:
    number = 0
    for prime in primes:
        prime_str = str(prime)
        # if not any([digit in (0, 2, 4, 5, 6, 8) for digit in prime_str]):
        cyclic_permutations = [
            (prime_str * 2)[(0 + i) : (len(prime_str) + i)]
            for i in range(1, len(prime_str))
        ]
        if all([int(b) in primes for b in cyclic_permutations]):
            number += 1
    return number


if __name__ == "__main__":
    print(circular_primes())
