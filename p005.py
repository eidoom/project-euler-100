#!/usr/bin/env python3
from cumulative_library import timing

LARGEST_DIVISOR = 20


def find_smallest_evenly_divisable_multiple(step, largest_divisor):
    """
    Find the smallest positive multiple n of the integer step which evenly divides (i.e. without remainder) by all the
    integers from one up to largest_divisor.
    :param step:
    :param largest_divisor:
    :return: n
    """
    n = step
    factors = list(range(2, largest_divisor + 1))
    for i, a in enumerate(factors):
        for b in factors[:i]:
            if not a % b:
                factors.remove(b)

    while any([n % i for i in factors]):
        n += step
    return n


@timing
def evenly_divisable(largest_divisor):
    """
    Finds the smallest positive number (answer) that is evenly divisible (divisable with no remainder) by all of the
    numbers from 1 to largest_divisor. Iterates up to largest divisor from divisor=1 to improve performance compared
    to find_smallest_multiple(1, largest_divisor), which gives the same answer.
    :param largest_divisor:
    :return: answer
    """
    divisor = 1
    answer = divisor
    while divisor <= largest_divisor:
        answer = find_smallest_evenly_divisable_multiple(answer, divisor)
        divisor += 1

    return answer


if __name__ == "__main__":
    print(evenly_divisable(LARGEST_DIVISOR))
