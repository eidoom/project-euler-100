#!/usr/bin/env python3

from cumulative_library import timing

LIMIT = 10000


def reverse_number(n):
    return int(str(n)[::-1])


def is_lychrel(n):
    count = 1
    while count < 50:
        a = n + reverse_number(n)
        if a == reverse_number(a):
            return False
        count += 1
        n = a
    return True


# @timing
# def lychrel_numbers_while(limit=LIMIT):
#     count = 0
#     n  = 1
#     while n <= limit:
#         if is_lychrel(n):
#             count += 1
#         n += 1
#     return count


# @timing
# def lychrel_numbers_for(limit=LIMIT):
#     count = 0
#     for n in range(1, limit + 1):
#         if is_lychrel(n):
#             count += 1
#     return count


@timing
def lychrel_numbers(limit=LIMIT):
    # return sum([1 for n in range(1, limit + 1) if is_lychrel(n)])
    return [is_lychrel(n) for n in range(1, limit + 1)].count(True)


if __name__ == "__main__":
    print(lychrel_numbers())
