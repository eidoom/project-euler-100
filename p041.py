#!/usr/bin/env python3

from itertools import permutations


def is_prime(n):
    for m in range(2, n):
        if not n % m:
            return False
    return n >= 2


if __name__ == "__main__":
    top = 8
    mid = 5
    print(
        max(
            filter(
                is_prime,
                (
                    sum(10 ** (i - 1) * i for i in range(mid, top))
                    + sum(10**i * b for i, b in enumerate(a))
                    for a in permutations(range(1, mid))
                ),
            )
        )
    )
