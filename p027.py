#!/usr/bin/env python3
from cumulative_library import timing, is_prime

LIMIT = 1000


@timing
def quadratic_primes(limit=LIMIT):
    max_a = 0
    max_b = 0
    max_n = 0
    for a in range(-limit + 1, limit):
        for b in range(-limit, limit + 1):
            n = 0
            while is_prime(n**2 + a * n + b):
                n += 1
            if n > max_n:
                max_a = a
                max_b = b
                max_n = n
    return max_a * max_b


if __name__ == "__main__":
    print(quadratic_primes())
