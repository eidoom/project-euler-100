#!/usr/bin/env python3
from cumulative_library import timing, product

POWER_LIMIT = 6


@timing
def champernownes_constant(power_limit=POWER_LIMIT):
    fractional_part = "".join([str(x) for x in range(1, 10**power_limit + 1)])
    return product([int(fractional_part[10**i - 1]) for i in range(power_limit + 1)])


if __name__ == "__main__":
    print(champernownes_constant())
