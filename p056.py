#!/usr/bin/env python3

from cumulative_library import timing

LIMIT = 100


@timing
def powerful_digit_sums(limit=LIMIT):
    max = 0
    for a in range(1, limit + 1):
        for b in range(1, limit + 1):
            c = sum([int(n) for n in list(str(a**b))])
            if c > max:
                max = c
    return max


if __name__ == "__main__":
    print(powerful_digit_sums())
