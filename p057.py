#!/usr/bin/env python3

from sys import setrecursionlimit
from fractions import Fraction

from cumulative_library import timing

# Slow: 10s - write fractions on paper to see num/dem pattern

LIMIT = 1000


# cache = {}


@timing
def square_root_convergents(limit=LIMIT):
    count = 0
    for d in range(1, limit + 1):
        # if d not in cache.keys():
        #     cache[d] = rec(d)
        # num = cache[d]
        num = rec(d)
        if len(str(num.numerator)) > len(str(num.denominator)):
            count += 1
    return count


def rec(max_depth, n=1, iteration=1):
    # try:
    #     return cache[max_depth]
    # except KeyError:
    if iteration == max_depth:
        return n + Fraction(1, 2)
    return n + Fraction(1, rec(max_depth, 2, iteration + 1))


if __name__ == "__main__":
    setrecursionlimit(1500)
    print(square_root_convergents())
    # print(cache)
