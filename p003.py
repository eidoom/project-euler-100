#!/usr/bin/env python3
from cumulative_library import timing

# composite number
# TEST_NUM = 2 * 1009 * 982451653
NUM = 600851475143


@timing
def largest_prime_factor(n=NUM):
    factor = 2
    while not n % factor:
        n = n / factor
    factor -= 1
    while n != 1:
        factor += 2
        while not n % factor:
            n = n / factor

    return factor


if __name__ == "__main__":
    print(largest_prime_factor())
