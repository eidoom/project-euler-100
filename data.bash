#!/usr/bin/env bash

files=(
	p022_names
	p042_words
	p059_cipher
	p067_triangle
	p079_keylog
)

for file in ${files[@]}; do
	filename="${file}.txt"
	if [ ! -f "${filename}" ]; then
		wget "https://projecteuler.net/project/resources/${filename}"
	else
		echo "${filename} exists"
	fi
done
