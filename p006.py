#!/usr/bin/env python3
from cumulative_library import timing

LIMIT = 100


def sum_square_difference(n=LIMIT):
    sum_squares = sum([i**2 for i in range(1, n + 1)])
    square_sum = sum(range(1, n + 1)) ** 2
    return square_sum - sum_squares


def sum_square_difference_alt(n=LIMIT):
    return sum(
        [sum([i * (j - i / n) for j in range(1, n + 1)]) for i in range(1, n + 1)]
    )


@timing
def sum_square_difference_overview(n=LIMIT):
    square_sum = (n * (n + 1)) ** 2 / 4
    sum_squares = n * (2 * n + 1) * (n + 1) / 6
    return square_sum - sum_squares


if __name__ == "__main__":
    print(sum_square_difference_overview())
