#!/usr/bin/env python3
from string import ascii_uppercase

from cumulative_library import timing

PATH = "p042_words.txt"


def get_triangle_number(n):
    return n * (n + 1) // 2


def get_word_value(word):
    letter_dictionary = {
        let: num
        for let, num in zip(ascii_uppercase, range(1, len(ascii_uppercase) + 1))
    }
    return sum([letter_dictionary[letter] for letter in word])


@timing
def coded_triangle_numbers(path=PATH):
    with open(path) as file:
        words = eval(file.read())

    word_values = [get_word_value(word) for word in words]

    i, triangle_numbers = 0, [1]
    while triangle_numbers[i] < max(word_values):
        i += 1
        triangle_numbers += [get_triangle_number(i + 1)]

    return sum([1 for word_value in word_values if word_value in triangle_numbers])


if __name__ == "__main__":
    print(coded_triangle_numbers())
