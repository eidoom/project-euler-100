#!/usr/bin/env python3
from cumulative_library import timing

LIMIT = 10**6


@timing
def double_base_palindromes(limit=LIMIT):
    palindrome_sum = 0
    for n in range(limit):
        binary = str(bin(n))[2:]
        if str(n) == str(n)[::-1] and binary == binary[::-1]:
            palindrome_sum += n
    return palindrome_sum


if __name__ == "__main__":
    print(double_base_palindromes())
