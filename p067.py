#!/usr/bin/env python3

if __name__ == "__main__":
    with open("p067_triangle.txt", "r") as f:
        r = f.readlines()

    d = [[int(x) for x in l.strip().split()] for l in r]

    for i, v in enumerate(d[:-1], start=1):
        for j in range(i + 1):
            d[i][j] += max(v[max(0, j - 1) : j + 1])

    print(max(d[-1]))
