#!/usr/bin/env python3

from cumulative_library import timing, get_data

LIMIT = 0

PATH = "p079_keylog.txt"

# Solved manually


def get_numbers(data=get_data(PATH)):
    return [int(x) for x in data.split("\n") if x]


@timing
def calc(limit=LIMIT):
    data = get_data(PATH)
    l = get_numbers(data)
    return list(set(l))


if __name__ == "__main__":
    print(calc())
