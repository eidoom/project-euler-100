#!/usr/bin/env python3
from cumulative_library import timing

DIGITS = 3


@timing
def find_largest_palindrome():
    numbers = list(reversed(range(10 ** (DIGITS - 1), 10**DIGITS)))

    largest_palindrome = 0
    for i, a in enumerate(numbers):
        for b in numbers[: i + 1]:
            product = a * b
            if product > largest_palindrome and str(product) == str(product)[::-1]:
                largest_palindrome = product
    return largest_palindrome


if __name__ == "__main__":
    print(find_largest_palindrome())
