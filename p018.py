#!/usr/bin/env python3
from cumulative_library import timing

TEST = """3
          7 4
          2 4 6
          8 5 9 3"""

DATA = """75
          95 64
          17 47 82
          18 35 87 10
          20 04 82 47 65
          19 01 23 75 03 34
          88 02 77 73 07 63 67
          99 65 04 28 06 16 70 92
          41 41 26 56 83 40 80 70 33
          41 48 72 33 47 32 37 16 94 29
          53 71 44 65 25 43 91 52 97 51 14
          70 11 33 28 77 73 17 78 39 68 17 57
          91 71 52 38 17 14 91 43 58 50 27 29 48
          63 66 04 68 89 53 67 30 73 16 69 87 40 31
          04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"""


def triangle(data=DATA):
    return [[int(x) for x in row.split()] for row in data.split("\n")]


@timing
def maximum_path_sum(data=DATA):
    numbers = triangle(data)

    def find_max_sum(position=(0, 0), summation=numbers[0][0], max_summation=0):
        if position[0] == len(numbers) - 1:
            if summation > max_summation:
                max_summation = summation
        else:
            for shift in (0, 1):
                new_position = [sum(x) for x in zip(position, (1, shift))]
                number = numbers[new_position[0]][new_position[1]]
                new_summation = summation + number
                max_summation = find_max_sum(new_position, new_summation, max_summation)
        return max_summation

    return find_max_sum()


def maximum_path_sum_alt(data=DATA):
    numbers = triangle(data)

    def find_paths(position=(0, 0), path=[numbers[0][0]], paths=[]):
        if position[0] == len(numbers) - 1:
            paths += [path]
        else:
            for shift in (0, 1):
                new_position = [sum(x) for x in zip(position, (1, shift))]
                number = numbers[new_position[0]][new_position[1]]
                new_path = path + [number]
                find_paths(new_position, new_path, paths)
        return paths

    return max([sum(x) for x in find_paths()])


if __name__ == "__main__":
    print(maximum_path_sum())
