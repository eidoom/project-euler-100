#!/usr/bin/env python3
from cumulative_library import timing, is_prime, list_primes

# Slow: 1 minute


@timing
def truncatable_primes():
    primes = list_primes(739397)[4:]
    trunc_prime_sum = 0
    for prime in primes:
        n = len(str(prime))
        if all(
            [
                is_prime(int(str(prime)[start:end]))
                for start, end in zip(
                    [0] * (n - 1) + list(range(1, n)), list(range(1, n)) + [n] * (n - 1)
                )
            ]
        ):
            trunc_prime_sum += prime
    return trunc_prime_sum


if __name__ == "__main__":
    print(truncatable_primes())
