#!/usr/bin/env python3
from cumulative_library import timing, product, find_factorial

SIDE_LENGTH = 20


# side_length defined differently to question for this answer
def lattice_paths_recursive(side_length=SIDE_LENGTH + 1):
    def walker(position, routes=0):
        if position == end:
            routes += 1
        else:
            if position[0] == position[1]:
                new_position = [sum(x) for x in zip(position, directions[0])]
                routes += 2 * walker(new_position)
            else:
                for direction in directions:
                    new_position = [sum(x) for x in zip(position, direction)]
                    if new_position[1] < side_length and new_position[0] < side_length:
                        routes = walker(new_position, routes)
        return routes

    start = [0] * 2
    end = [side_length - 1] * 2
    directions = ((0, 1), (1, 0))

    return walker(start)


def lattice_paths_combinatorics(n=SIDE_LENGTH):
    return find_factorial(2 * n) // (find_factorial(n) ** 2)


@timing
def better_combinatorics(n=SIDE_LENGTH):
    return int(round(product([(n + i) / i for i in range(1, n + 1)]), 0))


if __name__ == "__main__":
    # print(lattice_paths_recursive(3))
    print(better_combinatorics())
