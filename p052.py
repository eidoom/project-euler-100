#!/usr/bin/env python3

from cumulative_library import timing

LIMIT = 6


@timing
def permuted_multiples(limit=LIMIT):
    x = 125874
    while True:
        if all([sorted(str(n * x)) == sorted(str(x)) for n in range(2, limit + 1)]):
            return x
        x += 1


if __name__ == "__main__":
    print(permuted_multiples())
