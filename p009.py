#!/usr/bin/env python3
from math import sqrt

from cumulative_library import timing

N = 1000


@timing
def special_pythagorean_triplet(n=N):
    limit = n
    numbers = list(range(1, limit + 1))
    for i, a in enumerate(numbers):
        for b in numbers[i + 1 :]:
            c = sqrt(a**2 + b**2)
            if c % 1 == 0.0 and a + b + int(c) == n:
                return a * b * int(c)


if __name__ == "__main__":
    print(special_pythagorean_triplet())
