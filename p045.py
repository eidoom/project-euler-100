#!/usr/bin/env python3
from cumulative_library import timing

# Slow: 2 minutese

START = 144

triangular_numbers = {1: 1}
pentagonal_numbers = {1: 1}


def generate_triangular_numbers(lim):
    n = 1
    while triangular_numbers[n] < lim:
        n += 1
        if n not in triangular_numbers.keys():
            triangular_numbers[n] = n * (n + 1) // 2


def generate_pentagonal_numbers(lim):
    n = 1
    while pentagonal_numbers[n] < lim:
        n += 1
        if n not in pentagonal_numbers.keys():
            pentagonal_numbers[n] = n * (3 * n - 1) // 2


@timing
def check_hexagonal_numbers_for_tri_and_penta(n=START):
    while True:
        h = n * (2 * n - 1)
        generate_pentagonal_numbers(h)
        if h in pentagonal_numbers.values():
            generate_triangular_numbers(h)
            if h in triangular_numbers.values():
                return h
        n += 1


if __name__ == "__main__":
    print(check_hexagonal_numbers_for_tri_and_penta())
