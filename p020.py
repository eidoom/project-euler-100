#!/usr/bin/env python3
from cumulative_library import timing, find_factorial

TEST = 10
NUMBER = 100


def get_digits(n):
    return [n] if n < 10 else get_digits(n // 10) + [n % 10]


@timing
def sum_factorial_digits(n=NUMBER):
    return sum(get_digits(find_factorial(n)))


if __name__ == "__main__":
    print(sum_factorial_digits())
