#!/usr/bin/env python3

from cumulative_library import timing, product

# exclusive
LIMIT = 1000
# two factors
FACTORS = (3, 5)


def multiples(limit=LIMIT, factors=FACTORS):
    return sum([n for n in range(1, limit) if any([not n % i for i in factors])])


@timing
def multiples_alt(limit=LIMIT, factors=FACTORS):
    limit -= 1
    return sum([i * ((limit // i) * ((limit // i) + 1)) // 2 for i in factors]) - sum(
        [i * ((limit // i) * ((limit // i) + 1)) // 2 for i in (product(factors),)]
    )


if __name__ == "__main__":
    print(multiples_alt())
