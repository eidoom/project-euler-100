#!/usr/bin/env python3
from itertools import permutations

from cumulative_library import timing

DIGITS = list(range(10))
TEST_DIGITS = list(range(3))
INDEX = 10**6 - 1


@timing
def get_lexicographic_permutations(digits=DIGITS, i=INDEX):
    return "".join(map(str, list(permutations(sorted(digits)))[i]))


if __name__ == "__main__":
    print(get_lexicographic_permutations())
