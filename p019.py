#!/usr/bin/env python3
from cumulative_library import timing

FIRST_DATE = [1, 1, 1900]
START = [1, 1, 1901]
END = [31, 12, 2000]


def is_leap_year(year):
    if not year % 4:
        if not year % 100 and year % 400:
            return False
        else:
            return True
    else:
        return False


def get_days_in_year(year):
    if is_leap_year(year):
        return 366
    else:
        return 365


def get_days_in_month(month, year):
    if month == 2:
        if is_leap_year(year):
            return 29
        else:
            return 28
    elif month in (4, 6, 9, 11):
        return 30
    else:
        return 31


def get_days_in_date(day, month, year):
    return (
        day
        + sum([get_days_in_month(m, year) for m in range(1, month)])
        + sum([get_days_in_year(y) for y in range(FIRST_DATE[2], year)])
    )


def is_sunday(days):
    return not days % 7


@timing
def count_first_sundays(date=START, end=END):
    def iterate_days(limit, days_, first_sundays_):
        while date[0] <= limit:
            days_ += 1
            if is_sunday(days_) and date[0] == 1:
                first_sundays_ += 1
            date[0] += 1
        return days_, first_sundays_

    def iterate_months(limit, days_, first_sundays_):
        while date[1] <= limit:
            days_, first_sundays_ = iterate_days(
                get_days_in_month(date[1], date[2]), days_, first_sundays_
            )
            date[0] = 1
            date[1] += 1
        return days_, first_sundays_

    def iterate_years(limit, days_, first_sundays_):
        while date[2] < limit:
            days_, first_sundays_ = iterate_months(12, days_, first_sundays_)
            date[1] = 1
            date[2] += 1
        return days_, first_sundays_

    # def recurse_days(limit, days_, first_sundays_):
    #     if date[0] <= limit:
    #         return days_, first_sundays_
    #     else:
    #         days_ += 1
    #         if is_sunday(days_) and date[0] == 1:
    #             first_sundays_ += 1
    #         date[0] += 1
    #         return recurse_days(limit, days_, first_sundays_)

    first_sundays = 0
    days = get_days_in_date(*date) - 1

    days, first_sundays = iterate_years(end[2], days, first_sundays)
    days, first_sundays = iterate_months(end[1], days, first_sundays)
    days, first_sundays = iterate_days(end[0], days, first_sundays)

    return first_sundays


if __name__ == "__main__":
    print(count_first_sundays())
