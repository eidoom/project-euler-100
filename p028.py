#!/usr/bin/env python3
from cumulative_library import timing, make_number_spiral

# Should be an odd number
SIDE_LENGTH = 1001


@timing
def sum_number_spiral_diagonals(side_length=SIDE_LENGTH):
    grid = make_number_spiral(side_length)

    summation = -1
    for k in range(side_length):
        summation += grid[k][k] + grid[k][side_length - 1 - k]

    return summation


if __name__ == "__main__":
    print(sum_number_spiral_diagonals())
