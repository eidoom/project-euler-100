#!/usr/bin/env python3

from decimal import Decimal, getcontext

from cumulative_library import timing

# exclusive
LIMIT = 1000


def check_cycling(decimal, start, end):
    if 2 * end - start < len(decimal):
        if decimal[start:end] == decimal[end : 2 * end - start]:
            i = 0
            while (i + 3) * end - (i + 2) * start < len(decimal):
                i += end - start
                if (
                    decimal[(i + 1) * end - i * start : (i + 2) * end - (i + 1) * start]
                    != decimal[
                        (i + 2) * end
                        - (i + 1) * start : (i + 3) * end
                        - (i + 2) * start
                    ]
                ):
                    return False
            return True
    return False


def get_reciprocal_cycle_length(denominator):
    n = Decimal(1) / Decimal(denominator)
    if len(str(n)) > 17:
        decimal = str(n)[2:-1]
        for start in range(len(decimal)):
            for end in range(start + 1, len(decimal)):
                if check_cycling(decimal, start, end):
                    return end - start
    return 0


@timing
def get_max_length(lim=LIMIT):
    l_max = 0
    n_max = 0
    for n in range(3, lim, 2):
        if n % 5:
            l = get_reciprocal_cycle_length(n)
            if l > l_max:
                l_max = l
                n_max = n
    return f"1/{n_max} has reciprocal cycle length {l_max}."


if __name__ == "__main__":
    getcontext().prec = 2048
    print(get_max_length())
