#!/usr/bin/env python3
from cumulative_library import timing, list_primes

NUMBER = 10001


def sieve_auto_limit(j=NUMBER, n=2):
    try:
        return list_primes(10**n)[j - 1]
    except IndexError:
        return sieve_auto_limit(j, n + 1)


@timing
def main():
    return sieve_auto_limit()


if __name__ == "__main__":
    print(main())
