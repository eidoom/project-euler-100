#!/usr/bin/env python3
from cumulative_library import timing

POWER = 10**3


def power_digit_sum_pythonic(power=POWER):
    return sum([int(x) for x in str(2**power)])


def recursion(number, summation=0):
    return summation if not number else recursion(number // 10, summation + number % 10)


def loop(number, summation=0):
    while number:
        summation += number % 10
        number //= 10
    return summation


@timing
def power_digit_sum_mathematic(power=POWER):
    number = 2**power
    return recursion(number)
    # return loop(number)


if __name__ == "__main__":
    print(power_digit_sum_mathematic())
