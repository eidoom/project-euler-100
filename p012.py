#!/usr/bin/env python3

from math import sqrt, floor

from cumulative_library import timing, product, jth_prime_by_trial_division

LIMIT = 500


# def highly_divisible_triangular_number_recursive(i=3, triangular_number=3, limit=LIMIT):
#     # limit works from 3 upwards
#     if len([x for x in range(2, triangular_number // 2 + 1) if not triangular_number % x]) + 2 >= limit:
#         return triangular_number
#     return highly_divisible_triangular_number_recursive(i + 1, triangular_number + i, limit)


def find_prime_factorisation(n=LIMIT):
    factors = []
    factor = 2
    while not n % factor:
        factors.append(factor)
        n = n / factor
    factor -= 1
    while n != 1:
        factor += 2
        while not n % factor:
            factors.append(factor)
            n = n / factor
    return factors


def find_starting_point(n=LIMIT):
    prime_factors = list(reversed(find_prime_factorisation(n)))
    return product(
        [
            jth_prime_by_trial_division(i + 1) ** (prime_factors[i] - 1)
            for i in range(len(prime_factors))
        ]
    )


@timing
def highly_divisible_triangular_number(limit=LIMIT):
    starting_number = find_starting_point(limit)
    i = floor((sqrt(8 * starting_number + 1) - 1) / 2)
    triangular_number = i * (i + 1) // 2
    number_of_factors = 0
    while number_of_factors < limit:
        i += 1
        triangular_number += i
        bound = sqrt(triangular_number)
        # limit works from 3 upwards
        if triangular_number % 2:
            number_of_factors = 2 * (
                len(
                    [
                        x
                        for x in range(3, int(bound) + 1, 2)
                        if not triangular_number % x
                    ]
                )
                + 1
            )
        else:
            number_of_factors = 2 * (
                len(
                    [
                        x
                        for x in reversed(range(3, int(bound) + 1))
                        if not triangular_number % x
                    ]
                )
                + 2
            )
        if not bound % 1:
            number_of_factors -= 1
    return triangular_number


if __name__ == "__main__":
    print(highly_divisible_triangular_number())
