#!/usr/bin/env python3
from cumulative_library import find_greatest_common_divisor, timing


@timing
def digit_cancelling_fractions():
    numerator = 1
    denominator = 1
    for a in range(10):
        for i in range(0 if a else 1, a + 1):
            for j in range(a + 1 if a == i else 1, 10):
                if int(f"{i}{a}") / int(f"{a}{j}") == i / j:
                    numerator *= i
                    denominator *= j
    gcd = find_greatest_common_divisor(denominator, numerator)
    return denominator // gcd


if __name__ == "__main__":
    print(digit_cancelling_fractions())
