# python module

from functools import wraps
from math import sqrt
from time import perf_counter
from operator import mul
from functools import reduce


def product(numbers):
    """
    Analogous to the sum() built-in function but for the product rather than the sum.
    Equivalent to big pi notation in maths.
    :param numbers: List of numbers to take the total product of.
    :return: The total product of all the numbers in the list numbers.
    """
    return reduce(mul, numbers)


def is_prime(n):
    """
    Test whether integer number n is a prime number
    :param n: Integer number to test
    :return: Boolean statement of result (True if prime, False if composite)
    """
    for m in range(2, n):
        if not n % m:
            return False
    if n < 2:
        return False
    return True


def jth_prime_by_trial_division(j):
    """
    Find the jth prime using the trial division method
    :param j: denotes jth prime
    :return: jth prime
    """
    if j == 1:
        return 2
    i = 1
    n = 1
    while i < j:
        n += 2
        if is_prime(n):
            i += 1
    return n


def list_primes(upper_bound):
    """
    List all primes below upper_bound using sieve_of_eratosthenes method
    :param upper_bound: only primes below this integer number will be listed
    :return: List of all primes of value less than upper_bound
    """
    sieved = [True] * upper_bound
    for number in range(1, int(sqrt(upper_bound) + 1)):
        if sieved[number - 1]:
            if is_prime(number):
                n = number
                while n * number <= upper_bound:
                    sieved[n * number - 1] = False
                    n += 1
            else:
                sieved[number - 1] = False
    return [i for i, truth in enumerate(sieved, start=1) if truth]


def get_fibonacci(n, cache={}):
    if n <= 1:
        return n
    else:
        for index in (n - 1, n - 2):
            if index not in cache.keys():
                cache[index] = get_fibonacci(index, cache)

        return cache[n - 1] + cache[n - 2]


def find_factorial(n):
    """
    Finds n! using recursive method
    :param n: Number to find factorial of
    :return: n factorial
    """
    return 1 if n < 1 else n * find_factorial(n - 1)


def find_combinations(n, r):
    """
    Find n take r
    :param n:
    :param r:
    :return: C^n_r
    """
    return find_factorial(n) / (find_factorial(r) * find_factorial(n - r))


def find_proper_divisors(number, sort=False):
    """
    Find all the proper divisors of number
    :param number: number to find proper divisors of
    :return: list of number's proper divisors
    """
    proper_divisors = [1]
    limit = sqrt(number)

    if not number % 2:
        proper_divisors.extend([2, number // 2])
        j = 1
    else:
        j = 2

    for i in range(3, int(limit) + 1, j):
        if not number % i:
            proper_divisors.extend([i, number // i])

    if not limit % 1:
        proper_divisors.remove(int(limit))

    # proper_divisors = list(set(proper_divisors))

    return sorted(proper_divisors) if sort else proper_divisors


def find_divisor_pairs(number):
    """
    Find all the pairs of divisors of number
    :param number: number to find divisors pairs of
    :return: list of number's divisors pairs
    """
    divisor_pairs = [(1, number)]
    limit = sqrt(number)

    if not number % 2:
        divisor_pairs.append((2, number // 2))
        j = 1
    else:
        j = 2

    for i in range(3, int(limit) + 1, j):
        if not number % i:
            divisor_pairs.append((i, number // i))

    return divisor_pairs


def sum_proper_divisors(number):
    """
    Sum all the proper divisors of number
    :param number: number to find proper divisors of
    :return: summation of number's proper divisors
    """
    return sum(find_proper_divisors(number))


def find_greatest_common_divisor(a, b):
    """
    Find the greatest common divisor of two integers, a and b
    :param a: First integer, a
    :param b: Second integer, b
    :return: The greatest common divisor of a and b
    """
    if b > a:
        a, b = b, a
    gcd = 1
    for i in range(1, b + 1):
        if not a % i and not b % i:
            gcd = i
    return gcd


def are_duplicates(l, n):
    # len(set(l))==len(l)
    check = [str(i) for i in range(1, n + 1)]
    done = []
    for x in l:
        if x in check:
            if x in done:
                return True
            done.append(x)
    return False


def includes_digits(l, n):
    return all([str(i) in l for i in range(1, n + 1)])


def is_pandigital(l, n):
    return len(l) == n and includes_digits(l, n) and not are_duplicates(l, n)


def make_number_spiral(side_length, clockwise=True):
    grid = [[None for _ in range(side_length)] for _ in range(side_length)]

    n = 1
    i, j = side_length // 2, side_length // 2
    grid[i][j] = n

    for l in range(1, side_length - 1, 2):
        for _ in range(l):
            n += 1
            j += 1
            grid[i][j] = n

        for _ in range(l):
            n += 1
            i += 1 if clockwise else -1
            grid[i][j] = n

        for _ in range(l + 1):
            n += 1
            j -= 1
            grid[i][j] = n

        for _ in range(l + 1):
            n += 1
            i += -1 if clockwise else 1
            grid[i][j] = n

    for _ in range(side_length - 1):
        n += 1
        j += 1
        grid[i][j] = n

    return grid


def get_data(path):
    with open(path) as file:
        return file.read()


def time_formatter(start, rounding=1):
    """
    Generates a string telling the time passed since start, with automatic unit scaling.
    :param start: The initial time.
    :param rounding: The number of decimal points to include for the returned time.
    :return: The elapsed time since start, with automatic units.
    """
    time = perf_counter() - start
    power = 0
    unit = ""
    for index, prefix in zip(range(-3, 10, 3), ("kilo", "", "milli", "micro", "nano")):
        if time * (10**index) < 10**3:
            power = index
            unit = prefix
    return f"{round(10 ** power * time, rounding)} {unit}seconds"


def timing(function):
    """
    Decorator that returns function and prints how long it took to evaluate.
    :param function: A function, e.g. function(), that returns some solution.
    :return: The function solution (and print the time it took to run).
    """

    @wraps(function)
    def wrapped_function(*args, **kwargs):
        start = perf_counter()
        solution = function(*args, **kwargs)
        print(f"Function {function.__name__} took {time_formatter(start)}.")
        return solution

    return wrapped_function
