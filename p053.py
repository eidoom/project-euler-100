#!/usr/bin/env python3

from cumulative_library import find_combinations, timing

LIMIT = 100
BOUNDARY = 1e6

# Store factorials in look-up table (memoise) and use symmetry of Pascal's triangle to improve


@timing
def combinaoric_selections(limit=LIMIT, boundary=BOUNDARY):
    number = 0
    for n in range(1, limit + 1):
        for r in range(1, n + 1):
            if find_combinations(n, r) > boundary:
                number += 1
    return number


# def calc_alt(limit=LIMIT, boundary=BOUNDARY):
#     number = 0
#     n = 1
#     while n <= limit:
#         r = 1
#         while r <= n:
#             if find_combinations(n, r) > boundary:
#                 number += 1
#             r += 1
#         n += 1
#     return number


if __name__ == "__main__":
    print(combinaoric_selections())
