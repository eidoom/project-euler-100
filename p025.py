#!/usr/bin/env python3
from cumulative_library import timing, get_fibonacci

DIGITS = 1000


def get_first_n_digit_fibonacci(digits=1, i=1, n=DIGITS):
    return (
        i
        if digits == n
        else get_first_n_digit_fibonacci(len(str(get_fibonacci(i + 1))), i + 1, n)
    )


@timing
def get_first_n_digit_fibonacci_alt(digits=1, i=1, n=DIGITS):
    while digits < n:
        i += 1
        digits = len(str(get_fibonacci(i)))
    return i


if __name__ == "__main__":
    print(get_first_n_digit_fibonacci_alt())
