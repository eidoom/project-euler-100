#!/usr/bin/env python3
from cumulative_library import timing

# exclusive
LIMIT = 10**6


@timing
def longest_collatz_sequence(limit=LIMIT):
    max_chain_length = 0
    for starting_number in range(1, limit):
        n = starting_number
        chain_length = 1
        while n != 1:
            if n % 2:
                n = 3 * n + 1
            else:
                n = n // 2
            chain_length += 1
        if chain_length > max_chain_length:
            max_chain_length = chain_length
            max_starting_number = starting_number
    return max_starting_number


if __name__ == "__main__":
    print(longest_collatz_sequence())
