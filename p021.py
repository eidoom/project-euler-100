#!/usr/bin/env python3
from math import sqrt

from cumulative_library import timing, sum_proper_divisors

# exclusive
LIMIT = 10000

cache = {}


@timing
def amicable_numbers(limit=LIMIT):
    summation = 0
    for a in range(1, limit):
        if a not in cache.keys():
            cache[a] = sum_proper_divisors(a)
        b = cache[a]
        if b < a:
            if b not in cache.keys():
                cache[b] = sum_proper_divisors(b)
            if cache[b] == a:
                summation += a + b
    return summation


if __name__ == "__main__":
    print(amicable_numbers())
