#!/usr/bin/env python3
from cumulative_library import timing, find_factorial


@timing
def digit_factorials(limit=100000):
    summation = 0
    for n in range(3, limit + 1):
        if sum([find_factorial(int(i)) for i in str(n)]) == n:
            summation += n

    return summation


# def find_possibles(n):
#     print(sum([find_factorial(int(i)) for i in str(n)]))
#     print(len(str(sum([find_factorial(int(i)) for i in str(n)]))))


if __name__ == "__main__":
    print(digit_factorials())
