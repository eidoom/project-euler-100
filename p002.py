#!/usr/bin/env python3

from cumulative_library import timing, get_fibonacci

# inclusive
LIMIT = 4e6


def sum_even_fibonaccis(limit=LIMIT):
    n = 1
    current = get_fibonacci(n)
    total = 0
    while current <= limit:
        current = get_fibonacci(n)
        n += 1
        if not current % 2:
            total += current
    return total


def golfed_iterate(fibonacci=[1, 1], n=0, summation=0, limit=LIMIT):
    while fibonacci[n] + fibonacci[n + 1] <= limit:
        fibonacci += [fibonacci[n] + fibonacci[n + 1]]
        if not fibonacci[n + 2] % 2:
            summation += fibonacci[n + 2]
        n += 1
    return summation


def golfed_recurse(fibonacci=[1, 1], n=0, summation=0, limit=LIMIT):
    if fibonacci[n] + fibonacci[n + 1] > limit:
        return summation
    fibonacci += [fibonacci[n] + fibonacci[n + 1]]
    return golfed_recurse(
        fibonacci,
        n + 1,
        summation + (fibonacci[n + 2] if not fibonacci[n + 2] % 2 else 0),
    )


def golfed_full(fibonacci=[1, 1], n=0, summation=0, limit=LIMIT):
    return (
        summation
        if fibonacci[n] + fibonacci[n + 1] > limit
        else golfed_full(
            fibonacci + [fibonacci[n] + fibonacci[n + 1]],
            n + 1,
            summation
            + (
                fibonacci[n] + fibonacci[n + 1]
                if not (fibonacci[n] + fibonacci[n + 1]) % 2
                else 0
            ),
        )
    )


@timing
def main():
    # print(sum_even_fibonaccis())
    # print(golfed_iterate())
    print(golfed_recurse())
    # print(golfed_full())


if __name__ == "__main__":
    main()
