#!/usr/bin/env python3
from itertools import combinations

from cumulative_library import timing, sum_proper_divisors

LARGEST_NON_ABUNDANT_PAIR_SUM = 20161


def sum_divisors(number):
    return sum_proper_divisors(number)


# def is_abundant(number):
#     return sum_divisors(number) > number


# def get_abundant_numbers_dirty(upper_bound=LARGEST_NON_ABUNDANT_PAIR_SUM):
#     return [number for number in range(12, upper_bound + 1) if is_abundant(number)]


def get_class(number):
    if sum_divisors(number) > number:
        return True, 0  # abundant
    elif sum_divisors(number) == number:
        return True, 1  # perfect
    else:
        return False, None  # deficient


def get_abundant_numbers(upper_bound=LARGEST_NON_ABUNDANT_PAIR_SUM):
    sieve = [False] * upper_bound
    for number in range(12, upper_bound + 1):
        if not sieve[number - 1]:
            abundant_or_perfect, start = get_class(number)
            if abundant_or_perfect:
                n = 1 + start
                while n * number <= upper_bound:
                    sieve[n * number - 1] = True
                    n += 1
    return [i for i, truth in enumerate(sieve, start=1) if truth]


@timing
def get_non_abundant_pair_sums(limit=LARGEST_NON_ABUNDANT_PAIR_SUM):
    abundant_numbers = get_abundant_numbers()
    non_abundant_pair_sums = [True] * limit
    for number in abundant_numbers:
        if number <= limit // 2:
            non_abundant_pair_sums[2 * number - 1] = False
    for pair in combinations(abundant_numbers, 2):
        summation = sum(pair)
        if summation <= limit:
            non_abundant_pair_sums[summation - 1] = False
    return sum([i for i, truth in enumerate(non_abundant_pair_sums, start=1) if truth])


if __name__ == "__main__":
    print(get_non_abundant_pair_sums())
