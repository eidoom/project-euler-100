#!/usr/bin/env python3
from cumulative_library import timing, list_primes

LIMIT = 2 * 10**6


@timing
def summation_of_primes(limit=LIMIT):
    return sum(list_primes(limit))


if __name__ == "__main__":
    print(summation_of_primes())
