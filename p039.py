#!/usr/bin/env python3
from math import sqrt

from cumulative_library import timing, find_greatest_common_divisor

LIMIT = 1000


def find_integer_right_triangles(p):
    num = 0
    for a in range(1, p + 1):
        for b in range(a, p + 1):
            c = sqrt(a**2 + b**2)
            if not c % 1 and a + b + int(c) == p:
                num += 1
    return num


def do_brute_force(limit=LIMIT):
    max_num = 0
    max_p = 0
    for p in range(12, limit + 1):
        num = find_integer_right_triangles(p)
        if num > max_num:
            max_num, max_p = num, p
    return max_p


def make_triple(m, n, k=1):
    # if n > m:
    #     m, n = n, m
    return k * (m**2 - n**2), k * (2 * m * n), k * (m**2 + n**2)


@timing
def maximise_number_of_solutions(limit=1000):
    ps = []
    for m in range(2, limit // 2 + 1):
        for n in range(1, m + 1):
            if find_greatest_common_divisor(m, n) == 1 and not (m % 2 and n % 2):
                k = 1
                p = sum(make_triple(m, n, k))
                while p < limit:
                    ps += [p]
                    k += 1
                    p = sum(make_triple(m, n, k))
    return max(set(ps), key=ps.count)


if __name__ == "__main__":
    print(maximise_number_of_solutions())
