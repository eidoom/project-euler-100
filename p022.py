#!/usr/bin/env python3
from string import ascii_uppercase

from cumulative_library import timing

PATH = "p022_names.txt"


def get_letter_value(letter):
    return {
        let: num
        for let, num in zip(ascii_uppercase, range(1, len(ascii_uppercase) + 1))
    }[letter]


@timing
def get_names_scores(path=PATH):
    with open(path) as f:
        names = sorted(list(eval(f.read())))
    return sum(
        [
            i * sum([get_letter_value(letter) for letter in name])
            for i, name in enumerate(names, start=1)
        ]
    )


if __name__ == "__main__":
    print(get_names_scores())
