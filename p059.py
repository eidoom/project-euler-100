#!/usr/bin/env python3

from itertools import permutations
from string import ascii_lowercase

from cumulative_library import get_data

PATH = "p059_cipher.txt"
KEY = "god"


# ^ = XOR, e.g. 65^42=107
# ord("A") = 65; chr(107)=k


def process(data):
    return [int(x) for x in data.replace("\n", "").split(",")]


def decode(cipher, key_ascii):
    key = [ord(x) for x in key_ascii]
    n = len(cipher) // len(key) + 1
    return [a ^ b for a, b in zip(cipher, key * n)]


def find_key():
    keys = ["".join(x) for x in permutations(ascii_lowercase, 3)]
    cipher = process(get_data(PATH))
    for key in keys:
        message = decode(cipher, key)
        decoded = "".join([chr(x) for x in message])
        if all([word in decoded for word in (" the ", "was")]):
            print(f"{key}\n{decoded}\n")


def sum_ascii_values(key=KEY):
    cipher = process(get_data(PATH))
    return sum(decode(cipher, key))


if __name__ == "__main__":
    # find_key()
    print(sum_ascii_values())
