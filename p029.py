#!/usr/bin/env python3
from cumulative_library import timing

LIMIT = 100


@timing
def get_number_of_distinct_powers(limit=LIMIT):
    all = set()
    for a in range(2, limit + 1):
        for b in range(2, limit + 1):
            all |= {a**b}
    return len(all)


if __name__ == "__main__":
    print(get_number_of_distinct_powers())
