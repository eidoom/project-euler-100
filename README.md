# [Project Euler 100](https://gitlab.com/eidoom/project-euler-100)

The first 100 problems of [Project Euler](https://projecteuler.net/) and their solutions are [allowed to be discussed elsewhere](https://projecteuler.net/about#publish), unlike the later problems, so this repository can be public.
