#!/usr/bin/env python3
from cumulative_library import timing

LIMIT = 1000


@timing
def sum_self_powers(limit=LIMIT):
    return str(sum([x**x for x in range(1, limit + 1)]))[-10:]


if __name__ == "__main__":
    print(sum_self_powers())
