#!/usr/bin/env python3

from cumulative_library import timing, find_divisor_pairs, is_pandigital

N = 9


@timing
def calc(n=N):
    m = 4 if n > 7 else 3
    pandigitals = []
    for i in range(0, 10 ** (n - m)):
        for pair in find_divisor_pairs(i):
            l = "".join([str(e) for e in pair] + [str(i)])
            if is_pandigital(l, n):
                pandigitals.append(i)
                break
    return sum(pandigitals)


if __name__ == "__main__":
    print(calc())
